import seats from '../puzzles/day11.js'

let seatsWork = [...seats]
let seatsCopy = []
let update = false
let add = 0
let i = 0
let height = seats.length
let width = seats[0].length
let rowI = null
let columnI = null
let stop = false

const updateSeats = () => {
  seatsCopy = [...seatsWork]
  seatsWork = []
  update = false

  seatsCopy.forEach((rowSeats, rowIndex) => {
    let subSeatsLocale = []

    rowSeats.forEach((seat, columnIndex) => {
      let adjacentSeats = []

      if (seat !== -1) {
        // left diag
        rowI = rowIndex - 1
        columnI = columnIndex - 1
        stop = false
        while (rowI >= 0 && columnIndex >= 0 && !stop) {
          adjacentSeats.push(seatsCopy[rowI--][columnI--])
          if (seatsCopy[rowI][columnI] === 1) {
            stop = true
          }
        }
        // top
        rowI = rowIndex - 1
        columnI = columnIndex
        stop = false
        while (rowI >= 0 && !stop) {
          adjacentSeats.push(seatsCopy[rowI--][columnI])
          if (seatsCopy[rowI][columnI] === 1) {
            stop = true
          }
        }
        // right diag
        rowI = rowIndex - 1
        columnI = columnIndex + 1
        stop = false
        while (rowI >= 0 && columnIndex < width && !stop) {
          adjacentSeats.push(seatsCopy[rowI--][columnI++])
          if (seatsCopy[rowI][columnI] === 1) {
            stop = true
          }
        }
        // right
        rowI = rowIndex
        columnI = columnIndex + 1
        stop = false
        while (columnIndex < width && !stop) {
          adjacentSeats.push(seatsCopy[rowI][columnI++])
          if (seatsCopy[rowI][columnI] === 1) {
            stop = true
          }
        }

        add = adjacentSeats.reduce((previousValue, currentValue) => {
          if (currentValue) {
            return previousValue + currentValue
          }

          return previousValue
        }, 0)

        if (seat === 0) {
          if (add === 0) {
            update = true
            subSeatsLocale.push(1)
          } else {
            subSeatsLocale.push(0)
          }
        } else {
          if (add >= 5) {
            update = true
            subSeatsLocale.push(0)
          } else {
            subSeatsLocale.push(1)
          }
        }
      } else {
        subSeatsLocale.push(-1)
      }
    })

    seatsWork.push(subSeatsLocale)
  })

  if (update) {
    i++
    return updateSeats()
  } else {
    return seatsWork.reduce((previousValue, currentValue) => {
      return previousValue + currentValue.reduce((previousValue2, currentValue2) => {
        if (currentValue2 === 1) {
          return ++previousValue2
        }

        return previousValue2
      }, 0)
    }, 0)
  }
}

export default () => {
  return updateSeats()
}
