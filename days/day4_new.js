import fs from 'fs'
import path from 'path'

const start = (new Date()).getTime()
const lines = fs.readFileSync(path.join(process.cwd(), '/data/day4.txt'), { encoding: 'utf8' })
                .split('\n\n')
                .filter(x => x)
const eyesColor = new Set(['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth'])

class Passport {
  /**
   * @param {String} line
   */
  constructor (line) {
    this._map = new Map()
    const items = line.split(/\s+/g)
                      .map(item => item.split(':'))
    items.forEach(item => {
      const [key, value] = item
      this._map.set(key, value)
    })
  }

  isValid () {
    return this.digitValidation(this._map.get('byr'), 1920, 2002) &&
      this.digitValidation(this._map.get('iyr'), 2010, 2020) &&
      this.digitValidation(this._map.get('eyr'), 2020, 2030) &&
      this.hgtIsValid(this._map.get('hgt')) &&
      this.hclIsValid(this._map.get('hcl')) &&
      this.eclIsValid(this._map.get('ecl')) &&
      this.pidIsValid(this._map.get('pid'))
  }

  digitValidation(input, from, to) {
    if (!/^\d{4}$/.test(input)) return false
    input = parseInt(input)
    return input >= from && input <= to
  }

  hgtIsValid (hgt) {
    const matches = /^(?<size>[0-9]+)(?<unity>(?:in|cm))$/.exec(hgt)
    if (matches === null) return false

    let { size, unity } = matches.groups
    size = parseInt(size)
    if (isNaN(size)) return false

    switch (unity) {
      case 'cm':
        return size >= 150 && size <= 193

      case 'in':
        return size >= 59 && size <= 76
    }
  }

  hclIsValid (eyr) {
    return /^#[a-f0-9]{6}$/.test(eyr)
  }

  eclIsValid (eyr) {
    return eyesColor.has(eyr)
  }

  pidIsValid (eyr) {
    return /^[0-9]{9}$/.test(eyr)
  }
}

let validPassports = 0
for (const line of lines) {
  const passport = new Passport(line)
  if (passport.isValid()) validPassports++
}

console.log(validPassports)

const stop = (new Date()).getTime()
console.log(`${stop - start}ms`)
