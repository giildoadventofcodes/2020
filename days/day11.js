import seats from '../puzzles/day11.js'

let seatsWork = [...seats]
let seatsCopy = []
let update = false
let add = 0
let i = 0

let RtCl = null
let RtCe = null
let RtCr = null
let ReCl = null
let ReCr = null
let RbCl = null
let RbCe = null
let RbCr = null

const updateSeats = () => {
  seatsCopy = [...seatsWork]
  seatsWork = []
  update = false

  seatsCopy.forEach((rowSeats, rowIndex) => {
    let subSeatsLocale = []

    rowSeats.forEach((seat, columnIndex) => {
      let adjacentSeats = []

      if (seat !== -1) {
        if (rowIndex - 1 >= 0) {
          RtCl = seatsCopy[rowIndex - 1][columnIndex - 1]
          adjacentSeats.push(RtCl !== -1 ? RtCl : 0)
          RtCe = seatsCopy[rowIndex - 1][columnIndex]
          adjacentSeats.push(RtCe !== -1 ? RtCe : 0)
          RtCr = seatsCopy[rowIndex - 1][columnIndex + 1]
          adjacentSeats.push(RtCr !== -1 ? RtCr : 0)
        }

        ReCl = seatsCopy[rowIndex][columnIndex - 1]
        adjacentSeats.push(ReCl !== -1 ? ReCl : 0)
        ReCr = seatsCopy[rowIndex][columnIndex + 1]
        adjacentSeats.push(ReCr !== -1 ? ReCr : 0)

        if (rowIndex + 1 < seatsCopy.length) {
          RbCl = seatsCopy[rowIndex + 1][columnIndex - 1]
          adjacentSeats.push(RbCl !== -1 ? RbCl : 0)
          RbCe = seatsCopy[rowIndex + 1][columnIndex]
          adjacentSeats.push(RbCe !== -1 ? RbCe : 0)
          RbCr = seatsCopy[rowIndex + 1][columnIndex + 1]
          adjacentSeats.push(RbCr !== -1 ? RbCr : 0)
        }

        add = adjacentSeats.reduce((previousValue, currentValue) => {
          if (currentValue) {
            return previousValue + currentValue
          }

          return previousValue
        }, 0)

        if (seat === 0) {
          if (add === 0) {
            update = true
            subSeatsLocale.push(1)
          } else {
            subSeatsLocale.push(0)
          }
        } else {
          if (add >= 4) {
            update = true
            subSeatsLocale.push(0)
          } else {
            subSeatsLocale.push(1)
          }
        }
      } else {
        subSeatsLocale.push(-1)
      }
    })

    seatsWork.push(subSeatsLocale)
  })

  if (update) {
    i++
    return updateSeats()
  } else {
    return seatsWork.reduce((previousValue, currentValue) => {
      return previousValue + currentValue.reduce((previousValue2, currentValue2) => {
        if (currentValue2 === 1) {
          return ++previousValue2
        }

        return previousValue2
      }, 0)
    }, 0)
  }
}

export default () => {
  return updateSeats()
}
