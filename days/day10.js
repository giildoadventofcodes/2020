import fs from 'fs'
import path from 'path'

const lines = [0, ...fs.readFileSync(path.join(process.cwd(), '/data/day10.txt'), { encoding: 'utf8' })
                       .split('\n')
                       .filter(x => x)
                       .map(x => parseInt(x))]
lines.sort((a, b) => a - b)
lines.push(lines[lines.length-1] + 3)

class Adapter {
  constructor (lines) {
    this.adapters = lines
    this.diff1 = 0
    this.diff3 = 0
  }

  test () {
    let lastAdapter = 0
    this.adapters.forEach(adapter => {
      const diff = adapter - lastAdapter

      if (diff > 3) {
        throw new Error('Different too high')
      }

      diff === 1 ? this.diff1++ : this.diff3++
      lastAdapter = adapter
    })
    return this.diff1 * this.diff3
  }

  issues (adapters, cache) {
    const token = adapters.join(',')
    if (token in cache) return cache[token]

    let result = 1
    for (let i = 1; i < adapters.length - 1; i++) {
      if (adapters[i+1] - adapters[i-1] <= 3) {
        const newAdaptersList = [adapters[i-1]].concat(adapters.slice(i+1))
        result += this.issues(newAdaptersList, cache)
      }
    }

    cache[token] = result
    return result
  }
}

const adaptersList = new Adapter(lines)
console.log(adaptersList.test())

const adaptersList2 = new Adapter(lines)
console.log(adaptersList2.issues(lines, {}))

// const list = [1, 2, 3, 4, 5, 6, 7]
// const lvl0 = ['1234567']
// const lvl1 = ['123456', '123457', '123467', '123567', '124567', '134567', '234567']
// const lvl2 = ['34567', '14567', '12567', '12367', '12347', '12345', '24567', '13567', '12467', '12357', '12346', '23567', '13467', '12457', '12356', '23467', '13457', '12456', '23457', '13456', '23456']
// const lvl3 = ['4567', '3567', '3467', '3457', '3456', '2567', '2467', '2457', '2456', '2367', '2357', '2356', '2347', '2346', '2345', '1567', '1467', '1457', '1456', '1367', '1357', '1356', '1347', '1346', '1345', '1267', '1257', '1256', '1247', '1246', '1245', '1237', '1236', '1235', '1234']
//
// console.log(lvl0.length, lvl1.length, lvl2.length, lvl3.length)

