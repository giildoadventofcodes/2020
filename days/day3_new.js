import fs from 'fs'
import path from 'path'

const start = (new Date()).getTime()
const lines = fs.readFileSync(path.join(process.cwd(), '/data/day3.txt'), { encoding: 'utf8' })
                .split('\n')
                .filter(x => x)

class Map {
  constructor (map) {
    this._map = map
  }

  /**
   * @param {Number} x
   * @param {Number} y
   */
  getPosition (x, y) {
    return this._map[y][x % this.getMapWidth()]
  }

  getMapHeight () {
    return this._map.length
  }

  getMapWidth () {
    return this._map[0].length
  }
}

const map = new Map(lines.map(line => [...line]))

const trySlop = (dx, dy) => {
  let x = 0
  let y = 0
  let trees = 0

  while (y < map.getMapHeight()) {
    if (map.getPosition(x, y) === '#') trees++
    x += dx
    y += dy
  }

  return trees
}

const slopes = [[1, 1], [3, 1], [5, 1], [7, 1], [1, 2]]

console.log(slopes.reduce((previousValue, currentValue) => {
  return previousValue * trySlop(...currentValue)
}, 1))

const stop = (new Date()).getTime()
console.log(`${stop - start}ms`)
