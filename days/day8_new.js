import fs from 'fs'
import path from 'path'

const start = (new Date()).getTime()
const instructions = fs.readFileSync(path.join(process.cwd(), '/data/day8.txt'), { encoding: 'utf8' })
                       .split('\n')
                       .filter(x => x)

class Program {
  constructor (instructions) {
    this.instructions = instructions.map(line => {
      const { instruction, value } = /^(?<instruction>(?:acc|jmp|nop)) (?<value>(?:\+|-)[0-9]+)$/.exec(line).groups
      return { instruction, value: parseInt(value) }
    })
    this.accumulator = 0
    this.pointer = 0
    this.done = new Set()
    this.stop = false
    this.instruction = null
  }

  readProgram () {
    while (!this.stop) {
      const {instruction, value} = this.instructions[this.pointer]
      if (this.done.has(this.pointer)) break

      this.done.add(this.pointer)

      switch (instruction) {
        case 'jmp':
          this.pointer += value
          break

        case 'acc':
          this.accumulator += value
          this.pointer++
          break

        case 'nop':
          this.pointer++
          break
      }
    }
  }
}

const program = new Program(instructions)
program.readProgram()
console.log(program.accumulator)

const stop = (new Date()).getTime()
console.log(`${stop - start}ms`)
