import fs from 'fs'
import path from 'path'

const lines = fs.readFileSync(path.join(process.cwd(), '/data/day5.txt'), { encoding: 'utf8' })
                .split('\n')
                .filter(x => x)

class Seat {
  constructor (code) {
    code = [...code]
    this._id = this.parseCode(code.slice(0, 7)) * 8 + this.parseCode(code.slice(7))
  }

  parseCode (code) {
    return parseInt(code.map(char => char === 'B' || char === 'R' ? 1 : 0)
                        .join(''), 2)
  }

  get id () {
    return this._id
  }
}

let ids = []
for (const line of lines) {
  ids.push((new Seat(line)).id)
}
console.log(Math.max(...ids))

ids.sort((a, b) => a - b)

for (let i = 0; i < ids.length - 1; i++) {
  if (ids[i+1] - ids[i] > 1) {
    console.log(ids[i] + 1)
    break
  }
}
