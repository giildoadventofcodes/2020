import fs from 'fs'
import path from 'path'

const lines = fs.readFileSync(path.join(process.cwd(), '/data/day18.txt'), { encoding: 'utf8' })
                .split('\n')
                .filter(x => x)

class Calculator {
  static operators = {
    '*': (a, b) => parseInt(a) * parseInt(b),
    '+': (a, b) => parseInt(a) + parseInt(b),
  }

  constructor (lines) {
    this.calculs = [...lines]
    this.sum = 0
    this.i = 0
  }

  run () {
    this.calculs.forEach(calculation => {
      while (/\(/.test(calculation)) {
        calculation = this.replaceParentheses(calculation)
      }

      this.sum += parseInt(this.calculation(calculation))
    })

    return this.sum
  }

  calculation (calculation, p1 = '') {
    if (p1) calculation = p1
    let result = null

    while (/\+/.test(calculation)) {
      if (this) this.i++
      const {
        restBefore,
        firstNumber,
        operator,
        secondNumber,
        restAfter,
      } = /(?:(?<restBefore>.*) )?(?<firstNumber>\d+) (?<operator>\+) (?<secondNumber>\d+)(?<restAfter>.*)/.exec(calculation).groups
      result = Calculator.operators[operator](firstNumber, secondNumber)
      calculation = `${restBefore ? restBefore + ' ' : ''}${result}${restAfter}`
    }

    while (/\*/.test(calculation)) {
      if (this) this.i++
      const {
        firstNumber,
        operator,
        secondNumber,
        rest,
      } = /(?<firstNumber>\d+) (?<operator>\*) (?<secondNumber>\d+)(?<rest>.*)/.exec(calculation).groups
      result = Calculator.operators[operator](firstNumber, secondNumber)
      calculation = `${result}${rest}`
    }

    return calculation
  }

  replaceParentheses (calculation) {
    return calculation.replace(/\(([0-9 +*]+)\)/, this.calculation)
  }
}

const calculator = new Calculator(lines)
console.log(calculator.run())
console.log(calculator.i)
