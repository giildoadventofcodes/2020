import numbers from '../puzzles/day9.js'

let firstNumbers = []
let check = false

const checkNumbers = () => {
  return numbers.find((number, index) => {

    if (index < 25) {
      firstNumbers.push(number)
    } else {
      const numSelected = firstNumbers.find(num1 => {
        return firstNumbers.find(num2 => num1 !== num2 && (num1 + num2) === number) !== undefined
      })

      if (numSelected) {
        firstNumbers.shift()
        firstNumbers.push(number)
        check = false
        return false
      }
      return true
    }
  })
}

const wrongNumber = checkNumbers()
const newNumbers = numbers.slice(0, numbers.findIndex(number => number === wrongNumber))
let numberAdd = 0
let numbersAdd = []
let i = 0

newNumbers.find((number, index) => {
  numberAdd += number
  numbersAdd.push(number)
  i = index

  while (numberAdd < wrongNumber) {
    numberAdd += newNumbers[++i]
    numbersAdd.push(newNumbers[i])
  }

  if (numberAdd === wrongNumber) {
    return true
  }

  numberAdd = 0
  numbersAdd.splice(0, numbersAdd.length)
  i = 0
})

numbersAdd = numbersAdd.sort((a, b) => a - b)

export default () => {
  return numbersAdd[0] + numbersAdd[numbersAdd.length - 1]
}
