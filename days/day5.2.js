import boardingPass from '../puzzles/day5.js'

export default () => {
  let seatsNumber = []
  boardingPass.forEach(pass => {
    if (pass !== '') {
      let rowStart = [0, 127]
      let row = null
      let columnStart = [0, 7]
      let column = null

      for (const char of pass) {
        const diffRow = Math.ceil((rowStart[1] - rowStart[0]) / 2)
        const diffColumn = Math.ceil((columnStart[1] - columnStart[0]) / 2)

        if (char === 'F') {
          rowStart[1] -= diffRow
        } else if (char === 'B') {
          rowStart[0] += diffRow
        } else if (char === 'L') {
          columnStart[1] -= diffColumn
        } else if (char === 'R') {
          columnStart[0] += diffColumn
        }
      }

      if (rowStart[1] === rowStart[0]) {
        row = rowStart[0]
      } else {
        throw 'Row not found'
      }

      if (columnStart[1] === columnStart[0]) {
        column = columnStart[0]
      } else {
        throw 'Column not found'
      }

      seatsNumber.push(row * 8 + column)
    }
  })

  let mySeat = null
  seatsNumber.sort((a, b) => {
    if (a < b) {
      return -1
    } else if (a > b) {
      return 1
    } else {
      return 0
    }
  })
    .reduce((previousSeat, currentSeat, currentIndex) => {
      if ((previousSeat + 1) === currentSeat) {
        return currentSeat
      } else if ((previousSeat + 2) === currentSeat) {
        mySeat = currentSeat - 1
        return currentSeat
      } else {
        return currentSeat
      }
    }, null)

  return mySeat
}
