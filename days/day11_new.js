import fs from 'fs'
import path from 'path'

const lines = fs.readFileSync(path.join(process.cwd(), '/data/day11.txt'), { encoding: 'utf8' })
                .split('\n')
                .filter(x => x)
                .map(row => {
                  const rowArray = []
                  for (const char of row) {
                    switch (char) {
                      case '.':
                        rowArray.push('.')
                        break

                      case 'L':
                        rowArray.push(0)
                        break

                      case '#':
                        rowArray.push(1)
                        break
                    }
                  }

                  return rowArray
                })

class Hall {
  constructor (lines, options = {}) {
    this.options = options
    this.hallMap = [...lines]
    this.temporaryHallMap = []
    this.occupiedSeat = 0
    this.width = this.hallMap[0].length
    this.height = this.hallMap.length

    this.update = false
  }

  checkAdjacentSeats (x, y) {
    let occupiedSeat = 0
    for (let i = -1; i <= 1; i++) {
      for (let j = -1; j <= 1; j++) {
        if ((i !== 0 || j !== 0) &&
          x + j >= 0 &&
          x + j < this.width &&
          y + i >= 0 &&
          y + i < this.height &&
          this.hallMap[y + i][x + j] === 1
        ) {
          occupiedSeat++
        }
      }
    }

    return occupiedSeat
  }

  checkDirectionSeats (x, dx, y, dy) {
    let occupiedSeat = 0
    x += dx
    y += dy

    while (x >= 0 && x < this.width && y >= 0 && y < this.height) {
      const seat = this.hallMap[y][x]
      if (seat === 1) {
        occupiedSeat++
        break
      } else if (seat === 0) {
        break
      }
      x += dx
      y += dy
    }

    return occupiedSeat
  }

  updateSeatStatus (x, y) {
    const seat = this.hallMap[y][x]
    let occupiedSeat

    if (this.options.adjacent) {
      occupiedSeat = this.checkAdjacentSeats(x, y)
    } else {
      occupiedSeat = this.options.directions.reduce((accumulator, { x: dx, y: dy }) => {
        return accumulator + this.checkDirectionSeats(x, dx, y, dy)
      }, 0)
    }

    if (seat === 0 && occupiedSeat === 0) {
      this.update = true
      this.temporaryHallMap[y].push(1)
      this.occupiedSeat++
    } else if (this.hallMap[y][x] === 1 && occupiedSeat >= this.options.seatsOccupied) {
      this.update = true
      this.temporaryHallMap[y].push(0)
    } else {
      if (seat === 1) this.occupiedSeat++
      this.temporaryHallMap[y].push(seat)
    }
  }

  run () {
    for (let y = 0; y < this.hallMap.length; y++) {
      this.temporaryHallMap.push([])

      for (let x = 0; x < this.hallMap[y].length; x++) {
        this.updateSeatStatus(x, y)
      }
    }

    if (this.update) {
      this.reset()
      return this.run()
    } else {
      return this.occupiedSeat
    }
  }

  reset () {
    this.hallMap = [...this.temporaryHallMap]
    this.temporaryHallMap = []
    this.occupiedSeat = 0

    this.update = false
  }

  displayHall () {
    return this.hallMap.map(row => row.reduce((acc, cur) => acc += `${cur}`))
               .join('\n')
  }
}

// const hall = new Hall(lines, { adjacent: true, seatsOccupied: 4 })
//
// console.log(hall.run())

const directions = [
  { x: -1, y: -1 },
  { x: -1, y: 0 },
  { x: -1, y: 1 },
  { x: 0, y: -1 },
  { x: 0, y: 1 },
  { x: 1, y: -1 },
  { x: 1, y: 0 },
  { x: 1, y: 1 },
]

const hall = new Hall(lines, { directions, seatsOccupied: 5 })
console.log(hall.run())
