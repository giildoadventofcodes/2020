import passports from '../puzzles/day4.js'

export default () => {
  const splitCurrentValue = currentValue => {
    return currentValue.match(/(?:byr|iyr|eyr|hgt|hcl|ecl|pid|cid):[a-z0-9A-Z#]+/g)
                       .reduce((previousValue, currentValue) => {
                         const {
                           key,
                           value,
                         } = /((?<key>byr|iyr|eyr|hgt|hcl|ecl|pid|cid):(?<value>[a-z0-9A-Z#]+))/g.exec(currentValue).groups
                         previousValue[key] = value
                         return previousValue
                       }, {})
  }

  const requiredKeys = ['byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid']

  const checkPassport = passport => requiredKeys.every(requiredKey => {
    let value = null
    if (passport[requiredKey] === undefined) return false

    switch (requiredKey) {
      case 'byr':
        if (!/^[\d]{4}$/.test(passport[requiredKey])) return false
        value = parseInt(passport[requiredKey])
        return value >= 1920 && value <= 2002

      case 'iyr':
        if (!/^[\d]{4}$/.test(passport[requiredKey])) return false
        value = parseInt(passport[requiredKey])
        return value >= 2010 && value <= 2020

      case 'eyr':
        if (!/^[\d]{4}$/.test(passport[requiredKey])) return false
        value = parseInt(passport[requiredKey])
        return value >= 2020 && value <= 2030

      case 'hgt':
        const matches = /^(?<size>[0-9]+)(?<unity>(?:in|cm))$/.exec(passport[requiredKey])
        if (matches === null) return false
        let { size, unity } = matches.groups
        size = parseInt(size)
        if (isNaN(size)) return false

        switch (unity) {
          case 'cm':
            return size >= 150 && size <= 193

          case 'in':
            return size >= 59 && size <= 76
        }
        break

      case 'hcl':
        return /^#[a-f0-9]{6}$/.test(passport[requiredKey])

      case 'ecl':
        return ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth'].includes(passport[requiredKey])

      case 'pid':
        return /^[0-9]{9}$/.test(passport[requiredKey])
    }
  })

  let passportsValid = 0
  let passportsCheck = passports.reduce((previousValue, currentValue, index) => {
    if (index === 0) {
      previousValue.push(splitCurrentValue(currentValue))
      return previousValue
    } else if (currentValue !== '') {
      previousValue[previousValue.length - 1] = { ...previousValue[previousValue.length - 1], ...splitCurrentValue(currentValue) }
      return previousValue
    } else {
      if (checkPassport(previousValue[previousValue.length - 1])) passportsValid++
      previousValue.push([])
      return previousValue
    }
  }, [])

  if (checkPassport(passportsCheck[passportsCheck.length - 1])) passportsValid++

  return passportsValid
}
