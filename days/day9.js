import numbers from '../puzzles/day9.js'

let firstNumbers = []
let check = false

const checkNumbers = () => {
  return numbers.find((number, index) => {

    if (index < 25) {
      firstNumbers.push(number)
    } else {
      const numSelected = firstNumbers.find(num1 => {
        return firstNumbers.find(num2 => num1 !== num2 && (num1 + num2) === number) !== undefined
      })

      if (numSelected) {
        firstNumbers.shift()
        firstNumbers.push(number)
        check = false
        return false
      }
      return true
    }
  })
}

export default () => {
  return checkNumbers()
}
