import fs from 'fs'
import path from 'path'

const lines = fs.readFileSync(path.join(process.cwd(), '/data/day19.txt'), { encoding: 'utf8' })
                .split('\n')
                .filter(x => x)

/*
0: aaaabb | aaabab | abbabb | abbbab | aabaab | abaaab | aabbbb | ababbb
1: aaab | aaba | bbab | bbba | abaa | baaa | abbb | babb
2: aa | bb
3: ab | ba
4: a
5: b
 */

class Reader {
  constructor (lines) {
    this.instructions = new Map()
    this.finals = new Map()
    this.messages = new Set()

    lines.forEach(row => {
      if (/^[a-b]+$/.test(row)) {
        this.messages.add(row)
      } else {
        let { id, instruction } = /^(?<id>[0-9]+): (?<instruction>.*)$/.exec(row).groups
        id = parseInt(id)

        if (/^"[a|b]"$/.test(instruction)) {
          this.finals.set(id, /^"(?<value>[a|b])"$/.exec(instruction).groups.value)
        } else if (/\|/.test(instruction)) {
          const {
            instruction11,
            instruction12,
            instruction21,
            instruction22,
            instruction23,
          } = /^(?<instruction11>\d+)(?: (?<instruction12>\d+))? \| (?<instruction21>\d+)(?: (?<instruction22>\d+))?(?: (?<instruction23>\d+))?$/.exec(instruction).groups

          let instruction1 = [parseInt(instruction11)]
          if (instruction12) {
            instruction1.push(parseInt(instruction12))
          }
          let instruction2 = [parseInt(instruction21)]
          if (instruction22) {
            instruction2.push(parseInt(instruction22))
          }
          if (instruction23) {
            instruction2.push(parseInt(instruction23))
          }
          this.instructions.set(
            id,
            {
              double: true,
              completed: false,
              useForConversion: false,
              instructions: [instruction1, instruction2],
            })
        } else {
          const {
            instruction11,
            instruction12,
            instruction13,
          } = /^(?<instruction11>\d+)(?: (?<instruction12>\d+))?(?: (?<instruction13>\d+))?$/.exec(instruction).groups

          let instructions = [parseInt(instruction11)]

          if (instruction12) {
            instructions.push(parseInt(instruction12))
          }
          if (instruction13) {
            instructions.push(parseInt(instruction13))
          }
          this.instructions.set(
            id,
            {
              double: false,
              completed: false,
              useForConversion: false,
              instructions,
            })
        }
      }
    })
  }

  firstConversion () {
    this.finals.forEach((finalValue, finalKey) => {
      this.instructions.forEach((instructionValue, instructionKey) => {
        const instructionConverted = {
          useForConversion: false,
          double: instructionValue.double,
          instructions: this.conversion(
            instructionValue,
            finalKey,
            finalValue,
            true,
          ),
        }

        const completed = this.conversionCompleted(instructionConverted)
        if (completed && instructionValue.double) {
          instructionConverted.instructions = instructionConverted.instructions.join(' | ')
        }

        this.instructions.set(instructionKey, { ...instructionConverted, completed })
      })
    })
  }

  next () {
    this.instructions.forEach((instructionForConversion, keyForConversion) => {
      if (instructionForConversion.completed && !instructionForConversion.useForConversion) {
        this.instructions.forEach((instructionTested, keyTested) => {
          if (!instructionTested.completed) {
            const instructionConverted = {
              useForConversion: false,
              double: instructionTested.double,
              instructions: this.conversion(instructionTested, keyForConversion, instructionForConversion.instructions),
            }

            const completed = this.conversionCompleted(instructionConverted)
            if (completed && instructionTested.double) {
              instructionConverted.instructions = instructionConverted.instructions.join(' | ')
            }

            this.instructions.set(keyTested, { ...instructionConverted, completed })
          }
        })

        instructionForConversion.useForConversion = true
      }
    })
  }

  conversion ({ instructions, double, completed }, conversionKey, conversionValue) {
    if (typeof instructions === 'string') return instructions

    let newInstructions

    if (double) {
      newInstructions = instructions.map(instruction => {
        if (typeof instruction === 'string') return instruction

        const newInstruction = instruction.map(value => value === conversionKey ? conversionValue : value)

        if (newInstruction.every(value => typeof value === 'string')) {
          if (newInstruction.every(value => !/\|/.test(value))) {
            return newInstruction.join('')
          } else {
            return newInstruction.length === 1 ? newInstruction[0] : this.merge(newInstruction)
          }
        }

        return newInstruction
      })
    } else {
      newInstructions = instructions.map(value => value === conversionKey ? conversionValue : value)

      if (newInstructions.every(value => typeof value === 'string')) {
        if (newInstructions.every(value => !/\|/.test(value))) {
          return newInstructions.join('')
        } else {
          return newInstructions.length === 1 ? newInstructions[0] : this.merge(newInstructions)
        }
      }
    }

    return newInstructions
  }

  merge (instruction, last = false) {
    let inst = last ? new Set() : ''

    if (instruction.length === 2) {
      const cond1 = instruction[0].split(' | ')
      const cond2 = instruction[1].split(' | ')

      for (let i = 0; i < cond1.length; i++) {
        for (let j = 0; j < cond2.length; j++) {
          if (last) {
            inst.add(`${cond1[i]}`)
            inst.add(`${cond1[i]}${cond2[j]}`)
          } else {
            inst += `${cond1[i]}${cond2[j]}`

            if (i !== cond1.length - 1 || j !== cond2.length - 1) {
              inst += ` | `
            }
          }
        }
      }
    } else {
      const cond1 = instruction[0].split(' | ')
      const cond2 = instruction[1].split(' | ')
      const cond3 = instruction[2].split(' | ')

      for (let i = 0; i < cond1.length; i++) {
        for (let j = 0; j < cond2.length; j++) {
          for (let k = 0; k < cond3.length; k++) {
            if (last) {
              inst.add(`${cond1[i]}${cond3[k]}`)
              inst.add(`${cond1[i]}${cond2[j]}${cond3[k]}`)
            } else {
              inst += `${cond1[i]}${cond2[j]}${cond3[k]}`

              if (i !== cond1.length - 1 || j !== cond2.length - 1 || k !== cond3.length - 1) {
                inst += ` | `
              }
            }
          }
        }
      }
    }

    return inst
  }

  conversionCompleted ({ instructions }) {
    if (typeof instructions === 'string') return true
    return instructions.every(instruction => typeof instruction === 'string')
  }

  allCompleted () {
    let completed = true
    this.instructions.forEach((instruction, key) => {
      if (key !== 8 && key !== 11 && key !== 0) {
        if (!instruction.completed) completed = false
      }
    })

    return completed
  }

  infinityLoop () {
    this.next()

    const instruction11 = this.instructions.get(11)
    const cond42 = `(${instruction11.instructions[1][0].split(' | ')
                                                       .join('|')})`
    const cond31 = `(${instruction11.instructions[1][2].split(' | ')
                                                       .join('|')})`
    const pattern = `^${cond42}${cond42}*${cond42}${cond42}*${cond31}*${cond31}$`
    const regex = new RegExp(pattern)

    let correctAnswers = 0
    this.messages.forEach(value => {
      if (regex.test(value)) {
        correctAnswers++
      } else {
        console.log(value)
      }
    })

    return correctAnswers
  }

  run () {
    this.firstConversion()

    while (!this.allCompleted()) {
      this.next()
    }

    return this.infinityLoop()

    // return this.finish()
  }

  finish () {
    let correctMessage = 0
    let conditions = this.instructions.get(0).instructions

    if (!conditions instanceof Set) {
      conditions = new Set(conditions.split(' | '))
    }

    this.messages.forEach(value => {
      if (conditions.has(value)) correctMessage++
    })

    return correctMessage
  }
}

const reader = new Reader(lines)
console.log(reader.run())
