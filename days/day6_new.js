import fs from 'fs'
import path from 'path'

const groups = fs.readFileSync(path.join(process.cwd(), '/data/day6.txt'), { encoding: 'utf8' })
                 .split('\n\n')
                 .filter(x => x)

let total = 0
let total2 = 0

for (const group of groups) {
  const groupSet = new Set([...group.replace(/\n/g, '')])
  total += groupSet.size

  total2 += [...groupSet].filter(char => group.split('\n').filter(x => x).every(answers => answers.includes(char))).length
}

console.log(total)
console.log(total2)
