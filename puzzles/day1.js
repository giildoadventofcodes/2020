import fs from 'fs'
import path from 'path'

const readData = () => {
  return fs.readFileSync(path.join(process.cwd(), '/data/day1.txt'), { encoding: 'utf8' })
           .split('\n')
           .map(value => parseInt(value))
}

export default readData()
