import fs from 'fs'
import path from 'path'

const readData = () => {
  return fs.readFileSync(path.join(process.cwd(), '/data/day7.txt'), { encoding: 'utf8' })
           .split('\n')
           .reduce((rules, rule) => {
               const match = /^(?<color>[a-z]+ [a-z]+) bags contain (?<contain>.*)\.$/.exec(rule)
               if (match !== null && match.groups.contain !== 'no other bags') {
                 match.groups.contain = match.groups.contain.split(', ')
                                             .map(contain => {
                                               return /^(?<number>[0-9]+) (?<color>[a-z]+ [a-z]+) bags?$/.exec(contain).groups
                                             })
                 rules.push(match.groups)
               }

               return rules
           }, [])
}

export default readData()
