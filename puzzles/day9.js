import fs from 'fs'
import path from 'path'

const readData = () => {
  return fs.readFileSync(path.join(process.cwd(), '/data/day9.txt'), { encoding: 'utf8' })
           .split('\n')
           .filter(row => row !== '')
           .map(number => parseInt(number))
}

export default readData()
