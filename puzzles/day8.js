import fs from 'fs'
import path from 'path'

const readData = () => {
  return fs.readFileSync(path.join(process.cwd(), '/data/day8.txt'), { encoding: 'utf8' })
           .split('\n')
           .filter(row => row !== '')
           .map((row, index) => {
             let instructionReturn = { id: index }
             const { instruction, value } = /^(?<instruction>(?:acc|jmp|nop)) (?<value>(?:\+|-)[0-9]+)$/.exec(row).groups

             return {...instructionReturn, instruction, value: parseInt(value)}
           })
}

export default readData()
