import fs from 'fs'
import path from 'path'

const readData = () => {
  return fs.readFileSync(path.join(process.cwd(), '/data/day11.txt'), { encoding: 'utf8' })
           .split('\n')
           .filter(row => row !== '')
           .map(row => {
             const rowArray = []
             for (const char of row) {
               switch (char) {
                 case '.':
                   rowArray.push(-1)
                   break

                 case 'L':
                   rowArray.push(0)
                   break

                 case '#':
                   rowArray.push(1)
                   break
               }
             }

             return rowArray
           })
}

export default readData()
